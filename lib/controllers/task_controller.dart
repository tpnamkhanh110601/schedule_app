import 'package:get/get.dart';
import 'package:schedule_app/db/db_helper.dart';

import '../models/task.dart';

class TaskController extends GetxController {
  DateTime selectedDate = DateTime.now();
  @override
  void onReady() {
    super.onReady();
  }

  Future<int> addTask({Task? task}) async {
    return await DBHelper.insert(task);
  }

  Future<int> updateTask({Task? task}) async {
    return await DBHelper.update(task);
  }

  Future<List<Task>?> getListTask() async {
    return await DBHelper.getListTasks();
  }

  Future<List<Task>?> getTasksByDate(String? date) async {
    return await DBHelper.getTasksByDate(date);
  }
}
