import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import '../models/task.dart';

class DBHelper {
  static Database? _db;
  static const int _version = 1;
  static const String _tableName = 'tasks';

  static Future<void> initDb() async {
    if (_db != null) {
      return;
    }
    try {
      String _path = await getDatabasesPath() + 'tasks.db';
      _db =
          await openDatabase(_path, version: _version, onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE $_tableName('
          'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          'title STRING,'
          'note TEXT,'
          'date STRING,'
          'startTime STRING,'
          'endTime STRING,'
          'remind INTEGER,'
          'repeat STRING,'
          'color INTEGER,'
          'isCompleted INTEGER)',
        );
      });
    } catch (e) {
      print(e);
    }
  }

  static Future<int> insert(Task? task) async {
    return await _db?.insert(_tableName, task!.toJson()) ?? -1;
  }

  static Future<int> update(Task? task) async {
    return await _db?.update(
            _tableName,
            {
              'title': task?.title,
              'note': task?.note,
              'date': task?.date,
              'startTime': task?.startTime,
              'endTime': task?.endTime,
              'remind': task?.remind,
              'repeat': task?.repeat,
              'color': task?.color,
              'isCompleted': task?.isCompleted,
            },
            where: 'id = ?',
            whereArgs: [task?.id]) ??
        -1;
  }

  static Future<List<Task>?> getListTasks() async {
    final taskTable = await _db?.query(_tableName);
    return taskTable?.map((e) => Task.fromJson(e)).toList();
  }

  static Future<List<Task>?> getTasksByDate(String? date) async {
    final tasks =
        await _db?.query(_tableName, where: 'date = ?', whereArgs: [date]);
    return tasks?.map((e) => Task.fromJson(e)).toList();
  }
}
