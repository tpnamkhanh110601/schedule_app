import 'dart:async';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:schedule_app/controllers/task_controller.dart';
import 'package:schedule_app/models/task.dart';
import 'package:schedule_app/ui/theme.dart';
import 'package:schedule_app/widgets/input_field.dart';
import '../widgets/button.dart';

class EditPage extends StatefulWidget {
  Task? task;

  EditPage({
    super.key,
    required this.task,
  });

  @override
  State<EditPage> createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  final TaskController _taskController = Get.put(TaskController());
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _noteController = TextEditingController();
  bool isNotValidTitle = false;
  bool isNotValidNote = false;
  String _selectedDate = '';
  String _startTime = DateFormat('hh:mm a').format(DateTime.now()).toString();
  String _endTime = '9:30 PM';
  int _selectedRemind = 5;
  List<int> remindList = [
    5,
    10,
    15,
    20,
  ];

  String _selectedRepeat = 'None';
  List<String> repeatList = [
    'None',
    'Daily',
    'Weekly',
    'Monthly',
  ];
  int _selectedColor = 0;
  bool _isChecked = false;

  @override
  void initState() {
    super.initState();
    _initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _appBar(context),
        body: Container(
            padding: const EdgeInsets.only(left: 20, top: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Edit Task',
                    style: headingStyle,
                  ),
                  InputField(
                    title: 'Title',
                    hint: 'Enter your title',
                    controller: _titleController,
                  ),
                  isNotValidTitle
                      ? const Text(
                          'Your title is not valid',
                          style: TextStyle(color: Colors.red),
                        )
                      : const SizedBox(),
                  InputField(
                    title: 'Note',
                    hint: 'Enter your note',
                    controller: _noteController,
                  ),
                  isNotValidNote
                      ? const Text(
                          'Your note is not valid',
                          style: TextStyle(color: Colors.red),
                        )
                      : const SizedBox(),
                  InputField(
                    title: 'Date',
                    hint: _selectedDate,
                    widget: IconButton(
                      icon: const Icon(
                        Icons.calendar_today_outlined,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        _getDateFromUser();
                      },
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: InputField(
                          title: 'Start Time',
                          hint: _startTime,
                          widget: IconButton(
                              onPressed: () {
                                _getTimeFromUSer(isStartTime: true);
                              },
                              icon: const Icon(Icons.access_time_rounded,
                                  color: Colors.grey)),
                        ),
                      ),
                      Expanded(
                        child: InputField(
                          title: 'End Time',
                          hint: _endTime,
                          widget: IconButton(
                              onPressed: () {
                                _getTimeFromUSer(isStartTime: false);
                              },
                              icon: const Icon(Icons.access_time_rounded,
                                  color: Colors.grey)),
                        ),
                      ),
                    ],
                  ),
                  InputField(
                    title: 'Remind',
                    hint: '$_selectedRemind minutes early',
                    widget: DropdownButton(
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.grey,
                      ),
                      iconSize: 32,
                      elevation: 4,
                      style: subTitleStyle,
                      items:
                          remindList.map<DropdownMenuItem<String>>((int value) {
                        return DropdownMenuItem<String>(
                            value: value.toString(),
                            child: Text(value.toString()));
                      }).toList(),
                      underline: Container(
                        height: 0,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          _selectedRemind = int.parse(newValue!);
                        });
                      },
                    ),
                  ),
                  InputField(
                    title: 'Repeat',
                    hint: _selectedRepeat,
                    widget: DropdownButton(
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.grey,
                      ),
                      iconSize: 32,
                      elevation: 4,
                      style: subTitleStyle,
                      items: repeatList
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: const TextStyle(color: Colors.grey),
                            ));
                      }).toList(),
                      underline: Container(
                        height: 0,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          _selectedRepeat = newValue!;
                        });
                      },
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        'Completed',
                        style: titleStyle,
                      ),
                      Checkbox(
                          value: _isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              _isChecked = value ?? false;
                            });
                          }),
                    ],
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _colorPallete(),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: MyButton(
                          label: 'Edit Task',
                          onTap: () => _validateDate,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )));
  }

  _initData() {
    _titleController.text = widget.task?.title ?? '';
    _noteController.text = widget.task?.note ?? '';
    _selectedColor = widget.task?.color ?? 0;
    _selectedDate = widget.task?.date ?? '';
    _startTime = widget.task?.startTime ?? '';
    _endTime = widget.task?.endTime ?? '';
    _selectedRemind = widget.task?.remind ?? 5;
    _selectedRepeat = widget.task?.repeat ?? '';
    _isChecked = widget.task?.isCompleted == 1 ? true : false;
  }

  _updateTaskToDb() async {
    await _taskController.updateTask(
        task: Task(
            id: widget.task?.id,
            note: _noteController.text,
            title: _titleController.text,
            date: _selectedDate,
            startTime: _startTime,
            endTime: _endTime,
            remind: _selectedRemind,
            repeat: _selectedRepeat,
            color: _selectedColor,
            isCompleted: _isChecked ? 1 : 0));
  }

  _validateDate() {
    if (_titleController.text.isNotEmpty && _noteController.text.isNotEmpty) {
      _updateTaskToDb();
      Get.back();
    } else {
      if (_titleController.text.isEmpty) {
        setState(() {
          isNotValidTitle = true;
        });
      }
      if (_noteController.text.isEmpty) {
        setState(() {
          isNotValidNote = true;
        });
      }
    }
  }

  _appBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: context.theme.primaryColor,
      leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: Get.isDarkMode ? Colors.white : Colors.black,
        ),
      ),
      actions: const [
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/user_avatar.jpg'),
        ),
        SizedBox(
          width: 20,
        )
      ],
    );
  }

  _colorPallete() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Color',
          style: titleStyle,
        ),
        const SizedBox(
          height: 8,
        ),
        Wrap(
          children: List<Widget>.generate(
              3,
              (int index) => GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedColor = index;
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: CircleAvatar(
                        radius: 14,
                        backgroundColor: index == 0
                            ? primaryClr
                            : index == 1
                                ? pinkClr
                                : yellowClr,
                        child: _selectedColor == index
                            ? const Icon(
                                Icons.done,
                                color: Colors.white,
                              )
                            : const SizedBox(),
                      ),
                    ),
                  )),
        ),
      ],
    );
  }

  _getDateFromUser() async {
    DateTime? _pickerDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2124));

    if (_pickerDate != null) {
      setState(() {
        _selectedDate = DateFormat.yMd().format(_pickerDate);
      });
    }
  }

  _getTimeFromUSer({required bool isStartTime}) async {
    dynamic pickedTime = await _showTimePicker();
    try {
      var _formatedTime = await pickedTime.format(context).toString();
      if (pickedTime == null) {
        print('Time canceld');
      } else if (isStartTime) {
        setState(() {
          _startTime = _formatedTime;
        });
      } else if (isStartTime == false) {
        setState(() {
          _endTime = _formatedTime;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  _showTimePicker() async {
    return showTimePicker(
        initialEntryMode: TimePickerEntryMode.input,
        context: context,
        initialTime: TimeOfDay(
            hour: int.parse(_startTime.split(':')[0]),
            minute: int.parse(_startTime.split(':')[1].split(' ')[0])));
  }
}
