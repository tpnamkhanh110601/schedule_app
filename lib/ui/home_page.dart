import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:schedule_app/services/theme_services.dart';
import 'package:schedule_app/ui/edit_page.dart';
import 'package:schedule_app/ui/task_page.dart';
import 'package:schedule_app/ui/theme.dart';
import 'package:schedule_app/widgets/date_info.dart';
import 'dart:math' as math;

import '../controllers/task_controller.dart';
import '../models/task.dart';
import '../widgets/date_time_picker.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TaskController _taskController = Get.put(TaskController());
  List<Task>? listTask;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _addTaskBar(),
            _addDateBar(),
            _listTaskView(),
          ],
        ),
      ),
    );
  }

  void getListTask() async {
    var tasks = await _taskController.getListTask();
    if (tasks is List) {
      setState(() {
        listTask = tasks;
      });
    }
  }

  void getTasksByDate() async {
    String date = DateFormat.yMd().format(_taskController.selectedDate);
    var tasks = await _taskController.getTasksByDate(date);
    if (tasks is List) {
      setState(() {
        listTask = tasks;
      });
    }
  }

  _listTaskView() {
    getTasksByDate();
    if (listTask == null) return const SizedBox();
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: listTask?.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Get.to(EditPage(
              task: listTask?[index],
            ));
          },
          child: Container(
            padding: const EdgeInsets.only(top: 16, left: 16, bottom: 16),
            margin: const EdgeInsets.fromLTRB(20, 10, 20, 5),
            decoration: BoxDecoration(
                color: listTask?[index].color == 0
                    ? primaryClr
                    : listTask?[index].color == 1
                        ? pinkClr
                        : yellowClr,
                borderRadius: BorderRadius.circular(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const SizedBox(
                          width: 3,
                        ),
                        SizedBox(
                          width: 210,
                          child: Text(
                            '${listTask?[index].title}',
                            style: titleStyleWhite,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Icon(
                          Icons.timer_sharp,
                          color: white,
                        ),
                        Text(
                          '${listTask?[index].startTime} - ${listTask?[index].endTime}',
                          style: subTitleStyleWhite,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 3,
                        ),
                        SizedBox(
                          width: 210,
                          child: Text(
                            '${listTask?[index].note}',
                            style: subTitleStyleWhite,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Transform.rotate(
                  angle: -math.pi / 2,
                  child: Container(
                    decoration: const BoxDecoration(
                      border: Border(top: BorderSide(color: white)),
                    ),
                    alignment: Alignment.center,
                    width: 80,
                    height: 20,
                    child: listTask?[index].isCompleted == 0
                        ? Text(
                            'TODO',
                            style: fontSize12White,
                          )
                        : Text(
                            'COMPLETED',
                            style: fontSize12White,
                          ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  _appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: context.theme.primaryColor,
      leading: GestureDetector(
        onTap: () {
          ThemeService().switchTheme();
        },
        child: Icon(
          Get.isDarkMode ? Icons.wb_sunny_outlined : Icons.nightlight_round,
          size: 20,
          color: Get.isDarkMode ? Colors.white : Colors.black,
        ),
      ),
      actions: const [
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/user_avatar.jpg'),
        ),
        SizedBox(
          width: 20,
        )
      ],
    );
  }

  _addTaskBar() {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const DateInfo(),
          Container(
            height: 60,
            width: 130,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
            child: ElevatedButton(
              onPressed: () {
                Get.to(const TaskPage());
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: primaryClr,
                  foregroundColor: white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: const Text(
                '+ Add Task',
              ),
            ),
          )
        ],
      ),
    );
  }

  _addDateBar() {
    return const DateTimePicker();
  }
}
