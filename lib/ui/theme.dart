import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';

const Color bluishClr = Color(0xFF4e5ae8);
const Color yellowClr = Color(0xFFFFB746);
const Color pinkClr = Color(0xFFff4467);
const Color white = Colors.white;
const primaryClr = bluishClr;
const Color darkGreyClr = Color(0xFF121212);
const Color darkHeaderClr = Color(0xFF424242);

class Themes {
  static final light = ThemeData(
    primaryColor: white,
    brightness: Brightness.light,
  );

  static final dark =
      ThemeData(primaryColor: darkHeaderClr, brightness: Brightness.dark);
}

TextStyle get headingStyle {
  return TextStyle(
      fontSize: 30,
      fontWeight: FontWeight.bold,
      color: Get.isDarkMode ? white : Colors.black);
}

TextStyle get subheadingStyle {
  return TextStyle(
      fontSize: 22,
      fontWeight: FontWeight.bold,
      color: Get.isDarkMode ? Colors.grey[400] : Colors.grey);
}

TextStyle get titleStyle {
  return TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: Get.isDarkMode ? white : Colors.black);
}

TextStyle get subTitleStyle {
  return TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: Get.isDarkMode ? white : Colors.black);
}

TextStyle get titleStyleWhite {
  return const TextStyle(
      fontSize: 16, fontWeight: FontWeight.w400, color: white);
}

TextStyle get subTitleStyleWhite {
  return const TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, color: white);
}

TextStyle get fontSize12White {
  return const TextStyle(
    fontSize: 12,
    color: white,
  );
}

TextStyle get hintTextStyle {
  return const TextStyle(
      fontSize: 16, fontWeight: FontWeight.w400, color: Colors.grey);
}
