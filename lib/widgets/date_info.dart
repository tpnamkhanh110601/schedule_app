import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:schedule_app/ui/theme.dart';

class DateInfo extends StatefulWidget {
  const DateInfo({super.key});

  @override
  State<DateInfo> createState() => _DateInfoState();
}

class _DateInfoState extends State<DateInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          DateFormat.yMMMMd().format(DateTime.now()),
          style: subheadingStyle,
        ),
        Text(
          'Today',
          style: headingStyle,
        )
      ],
    );
  }
}
